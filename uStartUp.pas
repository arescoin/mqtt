unit uStartUp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.StdCtrls, Data.DB,
  ScSSHClient, ScSSHChannel, ScBridge, MemDS, DBAccess, Uni, JSON, UniProvider,
  MySQLUniProvider, WinSvc, IniFiles, EP_Misc, MemData;

type TEvent = Class
  public
    procedure HandleServerKeyValidate(Sender: TObject; NewServerKey: TScKey; var Accept: Boolean);
    procedure DBConnectionLost(Sender: TObject; Component: TComponent; ConnLostCause: TConnLostCause; var RetryMode: TRetryMode);
    procedure DBError(Sender: TObject; E: EDAError; var Fail: Boolean);
  end;

function StartServiceExecute(ServiceName: PChar): boolean;
function StopServiceExecute(ServiceName: PChar): boolean;

var
  Evt: TEvent;

implementation

function StopServiceExecute(ServiceName: PChar): boolean;
var
  SCH: SC_HANDLE;
  SvcSCH: SC_HANDLE;
  ss: SERVICE_STATUS;
  Count: integer;
begin
  Result := False;
  SCH := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCH = 0 then
    exit;

  try
    SvcSCH := OpenService(SCH, ServiceName, SERVICE_STOP or SERVICE_QUERY_STATUS);
    if SvcSCH = 0 then
    begin
      CloseServiceHandle(SCH);
      exit;
    end;

    try
      ControlService(SvcSCH, SERVICE_CONTROL_STOP, ss);
      Count := 0;
      repeat
        Sleep(500);
        QueryServiceStatus(SvcSCH, ss);
        Inc(Count);
      until (ss.dwCurrentState = SERVICE_CONTROL_STOP) or (Count >= 10);
      if ss.dwCurrentState = SERVICE_CONTROL_STOP then
        Result := True;
    finally
      CloseServiceHandle(SvcSCH);
    end;
  finally
    CloseServiceHandle(SCH);
  end;
end;

function StartServiceExecute(ServiceName: PChar): boolean;
var
  SCH: SC_HANDLE;
  SvcSCH: SC_HANDLE;
  ss: SERVICE_STATUS;
  p: PChar;
  Count: integer;
begin
  Result := False;
  SCH := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCH = 0 then
  begin
    CloseServiceHandle(SCH);
    exit;
  end;

  try
    Count := 0;
    SvcSCH := OpenService(SCH, ServiceName, SERVICE_START or SERVICE_QUERY_STATUS);
    if SvcSCH = 0 then
      exit;

    try
      StartService(SvcSCH, 0, p);
      repeat
        Sleep(500);
        QueryServiceStatus(SvcSCH, ss);
        Inc(Count);
      until (ss.dwCurrentState = SERVICE_RUNNING) or (Count >= 10);
      if ss.dwCurrentState = SERVICE_RUNNING then
        Result := True;
    finally
      CloseServiceHandle(SvcSCH);
    end;
  finally
    CloseServiceHandle(SCH);
  end;
end;


procedure TEvent.HandleServerKeyValidate(Sender: TObject; NewServerKey: TScKey; var Accept: Boolean);
begin
{<<Процедура проверки серверного ключа>>}
Accept := true;
{<<>>}
end;

procedure TEvent.DBConnectionLost(Sender: TObject; Component: TComponent; ConnLostCause: TConnLostCause; var RetryMode: TRetryMode);
begin
{<<Процедура возобновления подключения>>}
if ((ConnLostCause = clExecute) or (ConnLostCause = clConnect)) then
  begin
  RetryMode := rmReconnect;
  end else
if ((ConnLostCause = clOpen) or (ConnLostCause = clRefresh) or (ConnLostCause = clApply) or (ConnLostCause = clServiceQuery) or (ConnLostCause = clTransStart) or (ConnLostCause = clConnectionApply)) then
  begin
  RetryMode := rmReconnectExecute;
  end;
{<<>>}
end;

procedure TEvent.DBError(Sender: TObject; E: EDAError; var Fail: Boolean);
begin
{<<Процедура возобновления подключения>>}
Fail := false;
{<<>>}
end;

end.
