object MQTT: TMQTT
  OldCreateOrder = False
  DisplayName = 'MQTT Server Service'
  Interactive = True
  OnExecute = ServiceExecute
  OnStart = ServiceStart
  OnStop = ServiceStop
  Height = 306
  Width = 431
  object ScKey: TScRegStorage
    Left = 56
    Top = 16
  end
  object ScChn: TScSSHChannel
    Client = ScCli
    Left = 56
    Top = 72
  end
  object ScCli: TScSSHClient
    HostName = '92.242.45.209'
    Port = 50022
    User = 'srvadmin'
    Password = '**********'
    KeyStorage = ScKey
    Left = 56
    Top = 128
  end
  object MySQLUniProvider1: TMySQLUniProvider
    Left = 56
    Top = 192
  end
  object MQTTTimer: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = MQTTTimerTimer
    Left = 192
    Top = 120
  end
  object TMSMQTTClient1: TTMSMQTTClient
    Version = '1.0.2.0'
    Left = 328
    Top = 128
  end
end
