program MQTTService;

uses
  Vcl.SvcMgr,
  uService in 'uService.pas' {MQTT: TService},
  uStartUp in 'uStartUp.pas',
  EP_Misc in 'Units\EP_Misc.pas';

{$R *.RES}

begin
  // Windows 2003 Server requires StartServiceCtrlDispatcher to be
  // called before CoRegisterClassObject, which can be called indirectly
  // by Application.Initialize. TServiceApplication.DelayInitialize allows
  // Application.Initialize to be called from TService.Main (after
  // StartServiceCtrlDispatcher has been called).
  //
  // Delayed initialization of the Application object may affect
  // events which then occur prior to initialization, such as
  // TService.OnCreate. It is only recommended if the ServiceApplication
  // registers a class object with OLE and is intended for use with
  // Windows 2003 Server.
  //
  // Application.DelayInitialize := True;
  //

  if ParamStr(1) = '/uninstall' then
    StopServiceExecute('MQTTMainService');
  Application.Initialize;
  Application.CreateForm(TMQTT, MQTT);
  Application.Run;
  if ParamStr(1) = '/install' then
    StartServiceExecute('MQTTMainService');
end.
