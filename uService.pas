﻿unit uService;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs,
  Data.DB, DBAccess, Uni, MemDS, UniProvider, MySQLUniProvider,
  ScSSHClient, ScSSHChannel, ScBridge, IniFiles, EP_Misc, JSON, DateUtils,
  TMS.MQTT.Global, TMS.MQTT.Logging, TMS.MQTT.FileLogging, TMS.MQTT.Client,
  Vcl.ExtCtrls;

type
  Bit = 0..1;

type
  TBitSet = array[0..7] of Bit;

{
1 байт Тип пакета, для данного пакета = 1
1 байт Заряд батареи, %
1 байт Значения основных настроек (битовое поле)
4 байта Время снятия показаний, передаваемых в данном пакете
(unixtime UTC)
1 байт Температура, ⁰С
4 байта Показания на входе 1 (в зависимости от типа - число импульсов,
либо состояние 0 – разомкнут, 1 - замкнут)
4 байта Показания на входе 2 (в зависимости от типа - число
импульсов, либо состояние 0 – разомкнут, 1 - замкнут)
4 байта Показания на входе 3 (в зависимости от типа - число
импульсов, либо состояние 0 – разомкнут, 1 - замкнут)
4 байта Показания на входе 4 (в зависимости от типа - число
импульсов, либо состояние 0 – разомкнут, 1 – замкнут)
}
type TDataPackage = record
  PackageType,
  BatteryCharge,
  DataDateTime,
  Temperature,
  Data1,Data2,Data3,Data4: String;
  Settings: TBitSet;
end;

type TDeviceData = record
  DevEui,
  devName,
  lastDataTS,
  rssi,
  port,
  gatewayId,
  id_terminal,
  extid: String;
  rowdata: String;
  DataPackge: TDataPackage;
end;

type
  TMQTT = class(TService)
    ScKey: TScRegStorage;
    ScChn: TScSSHChannel;
    ScCli: TScSSHClient;
    MySQLUniProvider1: TMySQLUniProvider;
    MQTTTimer: TTimer;
    TMSMQTTClient1: TTMSMQTTClient;
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure DBConnectionError(Sender: TObject; E: EDAError;
      var Fail: Boolean);
    procedure MQTTClientConnectedStatusChanged(ASender: TObject;
      const AConnected: Boolean; AStatus: TTMSMQTTConnectionStatus);
    procedure MQTTClientPublishReceived(ASender: TObject; APacketID: Word;
      ATopic: string; APayload: TArray<System.Byte>);
    procedure MQTTTimerTimer(Sender: TObject);
  private
    { Private declarations }
    q: TUniQuery;
    DB: TUniConnection;
    UseSSH: Boolean;
    extid,p_extid: String;

    DeviceData: TDeviceData;
    LogFile: TFileName;

    ServerConnected, DatabaseConnected: Boolean;
    hHdl: NativeUInt;
    MQTTClient: TTMSMQTTClient;
    MQTTLogger: TTMSMQTTFileLogger;
    function CommonStartupProcedure: Boolean;
    procedure CommonShutdownProcedure;

    function ConnectToDB: Boolean;
    procedure DiconnectFromDB;
    procedure ConnectToServer;
    procedure DiconnectFromServer;
    procedure SetSubscribe;
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  MQTT: TMQTT;

implementation

{$R *.dfm}

uses uStartUp;

function GetNBit (X, N: byte): Bit;
begin
  try
    Result := x shr N and 1;
  except
    Result := 0;
  end;
end;

function GetBits (X: byte): TBitSet;
var
  N: integer;
begin
 for N := 0 to 7 do
   Result[N] := GetNBit(X, N);
end;

function MakeByte (BitSet: TBitSet): byte;
var
  i: integer;
begin
 Result := 0;
 for i := 7 downto 0 do
   Result := Result shl 1 + (BitSet[i] and 1);
end;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  MQTT.Controller(CtrlCode);
end;

procedure TMQTT.CommonShutdownProcedure;
begin
  DiconnectFromDB;
  DiconnectFromServer;
end;

function TMQTT.CommonStartupProcedure: Boolean;
begin
  ServerConnected := False;
  DatabaseConnected := False;
  extid := '';
  p_extid := '';

  LogFile := ExtractFilePath(ParamStr(0)) +
    Copy(ExtractFileName(ParamStr(0)), 1, Length(ExtractFileName(ParamStr(0))) - 3) +
    'log';

  ConnectToServer;
  Result := True;
end;

function TMQTT.ConnectToDB: Boolean;
var
  i: Integer;
  sssh: Boolean;
  ini: TIniFile;
begin
  Result := False;

//  MySQLUniProvider := TMySQLUniProvider.Create(nil);
  DB := TUniConnection.Create(nil);
  q := TUniQuery.Create(nil);
  q.Connection := DB;
  ScCli.KeyStorage := ScKey;
  ScCli.OnServerKeyValidate := Evt.HandleServerKeyValidate;
  ScCli.Options.TCPKeepAlive := True;

  ini := TIniFile.Create( ExtractFilePath(ParamStr(0)) +
    Copy(ExtractFileName(ParamStr(0)), 1, Length(ExtractFileName(ParamStr(0))) - 3) +
    'ini');

  try
    //Настройка подключения к БД процессинга
//    ToLog('Loading Connection to processing DB: ', LOG_STD, LogFile, FormatSettings);
    DB.ProviderName := 'MySQL';
    DB.Database := ini.ReadString('DB','Name','');
    DB.Server := ini.ReadString('DB','Server','');
    DB.Port := ini.ReadInteger('DB','Port',3306);
    DB.UserName := ini.ReadString('DB','User','');
    DB.Password := ini.ReadString('DB','Password','');

    DB.Options.LocalFailover := True;
    DB.OnError := DBConnectionError;

    DB.SpecificOptions.Values['Charset'] := ini.ReadString('DB','Charset','cp1251');

    UseSSH := Boolean(ini.ReadInteger('DB','UseSSH',0));

    if UseSSH then
    begin
      ScCli.HostName := ini.ReadString('DB','SSHHost','');
      ScCli.Port :=  ini.ReadInteger('DB','SSHPort',22);
      ScCli.User := ini.ReadString('DB','SSHUser','');
      ScCli.Password := ini.ReadString('DB','SSHPassword','');
      ScChn.Client := ScCli;
      ScChn.DestHost := DB.Server;
      ScChn.DestPort := DB.Port;
      ScChn.SourcePort := ScChn.DestPort + 1;
      DB.Port := ScChn.SourcePort;
    end;

    try
      //Инициализация подключения к БД процессинга
      ToLog('Connecting to processing DB...', LOG_STD, LogFile, FormatSettings);
      if UseSSH then
      begin
        i := 0;
        repeat
          try
            ScCli.Connect;
            ScChn.Connect;
            ToLog('SSH Connected: OK', LOG_STD, LogFile, FormatSettings);
          except
            Inc(i);
            ScChn.SourcePort := ScChn.SourcePort + 1;
            DB.Port := ScChn.SourcePort;
          end;
        until ((i = 20) or ((ScChn.Connected) and (ScCli.Connected)));
      end;

      DB.Connect;
      ToLog('Connecting to processing DB: OK', LOG_STD, LogFile, FormatSettings);
      Result := True;
    except
      Result := False;
      try
        ToLog('Connecting to processing DB: ERROR (' + IntToStr(GetLastError) + ')!', LOG_STD, LogFile, FormatSettings);
      except
      end;
    end;
  finally
    FreeAndNil(ini);
  end;
end;

procedure TMQTT.ConnectToServer;
var
  ini: TIniFile;
begin
  try
    ini := TIniFile.Create( ExtractFilePath(ParamStr(0)) +
      Copy(ExtractFileName(ParamStr(0)), 1, Length(ExtractFileName(ParamStr(0))) - 3) +
      'ini');

    MQTTLogger := TTMSMQTTFileLogger.Create(Self);
    MQTTLogger.Verbosity := vINFO;
    MQTTLogger.FileName := ExtractFilePath(ParamStr(0)) + 'MQTTLogger.log';
    MQTTClient := TTMSMQTTClient.Create(Self);
    MQTTClient.KeepAliveSettings.AutoReconnect := True;
    MQTTClient.OnConnectedStatusChanged := MQTTClientConnectedStatusChanged;
    MQTTClient.OnPublishReceived := MQTTClientPublishReceived;
    MQTTClient.Logger := MQTTLogger;
    try
       MQTTClient.BrokerHostName := ini.ReadString('Server','Host','');
       MQTTClient.BrokerPort := ini.ReadInteger('Server','Port',1883);
       ToLog('Connecting to host: ' + MQTTClient.BrokerHostName + ', port: ' + IntToStr(MQTTClient.BrokerPort), LOG_STD, LogFile, FormatSettings);
       if ini.ReadString('Server','User','') <> '' then
         MQTTClient.Credentials.Username := ini.ReadString('Server','User','');
       if ini.ReadString('Server','Password','') <> '' then
         MQTTClient.Credentials.Password := ini.ReadString('Server','Password','');
       ToLog('Connecting to broker...', LOG_STD, LogFile, FormatSettings);
       MQTTClient.Connect;
    finally
      FreeAndNil(ini);
    end;
  except
    on E: Exception do
      ToLog('Connecting to broker: ERROR - ' + E.Message, LOG_STD, LogFile, FormatSettings);
  end;
end;

procedure TMQTT.SetSubscribe;
var
  ini: TIniFile;
  s: String;
  TopicList: TStringList;
  i: Integer;
begin
  try
    ini := TIniFile.Create( ExtractFilePath(ParamStr(0)) +
      Copy(ExtractFileName(ParamStr(0)), 1, Length(ExtractFileName(ParamStr(0))) - 3) +
      'ini');
    TopicList := TStringList.Create;
    try
       s := ini.ReadString('Subscribe','Topic','');
       ToLog('Subscribers list from settings: ' + s, LOG_STD, LogFile, FormatSettings);
       TopicList.Delimiter := ',';
       TopicList.DelimitedText := s;

       for i := 0 to TopicList.Count - 1 do
       begin
         MQTTClient.Subscribe(TopicList[i]);
         ToLog('Subscriber set: ' + TopicList[i], LOG_STD, LogFile, FormatSettings);
       end;
    finally
      FreeAndNil(ini);
      FreeAndNil(TopicList);
    end;
  except
    on E: Exception do
      ToLog('Set Subscribers ERROR - ' + E.Message, LOG_STD, LogFile, FormatSettings);
  end;
end;

procedure TMQTT.DiconnectFromDB;
begin
  try
    //Отключение от БД процессинг
    ToLog('Disconnecting from processing DB...', LOG_STD, LogFile, FormatSettings);
    if (q.Active) then q.Close;
    q.Connection := nil;
    if (DB.Connected) then DB.Disconnect;
    if UseSSH then
    begin
      ScChn.Disconnect;
      ScCli.Disconnect;
    end;

    ToLog('Disconnecting from processing DB: OK', LOG_STD, LogFile, FormatSettings);

    //Освобождение памяти
    FreeAndNil(q);
    FreeAndNil(DB);
//    FreeAndNil(MySQLUniProvider);
  except
  end;
end;

procedure TMQTT.DiconnectFromServer;
begin
  try
    //Отключение от БД процессинг
    ToLog('Disconnecting from broker...', LOG_STD, LogFile, FormatSettings);
    if (MQTTClient.IsConnected) then MQTTClient.Disconnect;

    ToLog('Disconnecting from broker: OK', LOG_STD, LogFile, FormatSettings);

    //Освобождение памяти
    FreeAndNil(Evt);
  except
  end;
end;

function TMQTT.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TMQTT.MQTTClientConnectedStatusChanged(ASender: TObject;
  const AConnected: Boolean; AStatus: TTMSMQTTConnectionStatus);
var
  SStatus: String;
begin
  if AConnected then
  begin
    ServerConnected := True;
    ToLog('Connected to broker.', LOG_STD, LogFile, FormatSettings);
    DatabaseConnected := ConnectToDB;
    SetSubscribe;
  end else
  begin
    SStatus := 'Unknown Error';
    case AStatus of
      csNotConnected: SStatus := 'Not Connected';
      csConnectionRejected_InvalidProtocolVersion: SStatus := 'Connection Rejected Invalid Protocol Version';
      csConnectionRejected_InvalidIdentifier: SStatus := 'Connection Rejected Invalid Identifier';
      csConnectionRejected_ServerUnavailable: SStatus := 'Connection Rejected Server Unavailable';
      csConnectionRejected_InvalidCredentials: SStatus := 'Connection Rejected Invalid Credentials';
      csConnectionRejected_ClientNotAuthorized: SStatus := 'Connection Rejected Client Not Authorized';
      csConnectionLost: SStatus := 'Connection Lost';
      csConnecting: SStatus := 'Connecting';
      csReconnecting: SStatus := 'Reconnecting';
      csConnected: SStatus := 'Connected';
    end;
    ToLog('Not Connected to broker: ' + SStatus, LOG_STD, LogFile, FormatSettings);
//    if ServerConnected then
//    begin
//      ToLog('Try reconnecting to broker...', LOG_STD, LogFile, FormatSettings);
//      MQTTClient.Connect();
//    end else
//    begin
      ServerConnected := False;
//    end;
  end;
end;

procedure TMQTT.MQTTClientPublishReceived(ASender: TObject; APacketID: Word;
  ATopic: string; APayload: TArray<System.Byte>);
var
  i, j: Integer;
  s: String;
  JSONObject, JSONObject2: TJSONObject;
  JSONArray: TJSONArray;
  Pairs, Pairs2: TJSONPairEnumerator;
  chanel: String;
  bt: TBitSet;
  ch: Integer;
  jp,jp2: TJSONPair;
  FYear, FMonth, FDay, FHour, FMinute, FSecond, FMillisecond: Word;
  FDateTime: TDateTime;
begin

  try
    ToLog('Publish Received Topic: ' + ATopic, LOG_STD, LogFile, FormatSettings);
    //for i := 0 to High(APayload) do
    //Memo1.Lines.Add(APayload[i].ToString);
    SetString(s, PAnsiChar(@APayload[0]), Length(APayload));
    ToLog('Public Message: ' + s, LOG_STD, LogFile, FormatSettings);
    //{ "data": { "value": 393 }, "status": { "devEUI" : "02124b0016804c2d", "rssi": -39, "temperature": 0, "battery": 3600, "date": "2017-12-19T13:35:21.670909Z" }}

    if Pos('data', s) > 0 then
    begin
      DeviceData.rowdata := '';
      DeviceData.DataPackge.Data1 := '';
      DeviceData.DataPackge.Data2 := '';
      DeviceData.DataPackge.Data3 := '';
      DeviceData.DataPackge.Data4 := '';
      JsonObject := TJSONObject.ParseJSONValue(s) as TJSONObject;
      if Assigned(JsonObject) then
      begin
        extid := '';

        Pairs := JsonObject.GetEnumerator;
        DeviceData.id_terminal := '';
        try
          while Pairs.MoveNext do
          begin
            if Pairs.Current.JsonString.Value = 'data' then
            begin
              jp := Pairs.Current;
              JsonObject2 := TJSONObject.ParseJSONValue(jp.JsonValue.ToString) as TJSONObject;
              if Assigned(JsonObject2) then
              begin
                Pairs2 := JsonObject2.GetEnumerator;
                try
                  while Pairs2.MoveNext do
                  begin
                    if Pairs2.Current.JsonString.Value = 'value' then
                    begin
                      DeviceData.rowdata := Pairs2.Current.JsonValue.Value;
                    end else
                    if Pairs2.Current.JsonString.Value = 'btn' then
                    begin
                      DeviceData.rowdata := Pairs2.Current.JsonValue.Value;
                    end else
                    if Pairs2.Current.JsonString.Value = 'P1' then
                    begin
                      JSONArray := Pairs2.Current.JsonValue as TJSONArray;
                      for i := 0 to JSONArray.Count - 1 do
                        DeviceData.DataPackge.Data1 := JSONArray.Items[i].ToString;
                    end else
                    if Pairs2.Current.JsonString.Value = 'P2' then
                    begin
                      JSONArray := Pairs2.Current.JsonValue as TJSONArray;
                      for i := 0 to JSONArray.Count - 1 do
                        DeviceData.DataPackge.Data2 := JSONArray.Items[i].ToString;
                    end;
                  end;
                finally
                  Pairs2.Free;
                end;
              end;
            end;
            if Pairs.Current.JsonString.Value = 'status' then
            begin
              jp := Pairs.Current;
              JsonObject2 := TJSONObject.ParseJSONValue(jp.JsonValue.ToString) as TJSONObject;
              if Assigned(JsonObject2) then
              begin
                Pairs2 := JsonObject2.GetEnumerator;
                try
                  while Pairs2.MoveNext do
                  begin
                    if Pairs2.Current.JsonString.Value = 'devEUI' then
                    begin
                      DeviceData.DevEui := Pairs2.Current.JsonValue.Value;
                      DeviceData.devName := Pairs2.Current.JsonValue.Value;
                    end;
                    if Pairs2.Current.JsonString.Value = 'rssi' then
                    begin
                      DeviceData.rssi := Pairs2.Current.JsonValue.Value;
                    end;
                    if Pairs2.Current.JsonString.Value = 'temperature' then
                    begin
                      DeviceData.DataPackge.Temperature := Pairs2.Current.JsonValue.Value;
                    end;
                    if Pairs2.Current.JsonString.Value = 'battery' then
                    begin
                      DeviceData.DataPackge.BatteryCharge := Pairs2.Current.JsonValue.Value;
                    end;
                    if Pairs2.Current.JsonString.Value = 'date' then
                    begin
                      // 2017-12-19T13:35:21.670909Z
                      DeviceData.DataPackge.DataDateTime := Pairs2.Current.JsonValue.Value;
                      DeviceData.DataPackge.DataDateTime := Copy(DeviceData.DataPackge.DataDateTime, 1, 10) +
                        ' ' + Copy(DeviceData.DataPackge.DataDateTime, 12, 8);
                      FDateTime := IncHour(EncodeDateTime(StrToInt(Copy(DeviceData.DataPackge.DataDateTime, 1, 4)),
                                     StrToInt(Copy(DeviceData.DataPackge.DataDateTime, 6, 2)),
                                     StrToInt(Copy(DeviceData.DataPackge.DataDateTime, 9, 2)),
                                     StrToInt(Copy(DeviceData.DataPackge.DataDateTime, 12, 2)),
                                     StrToInt(Copy(DeviceData.DataPackge.DataDateTime, 15, 2)),
                                     StrToInt(Copy(DeviceData.DataPackge.DataDateTime, 18, 2)),
                                     0),3);

                      DeviceData.DataPackge.DataDateTime := FormatDateTime('yyyy-mm-dd hh:nn:ss', FDateTime);
                    end;
                  end;
                finally
                  Pairs2.Free;
                end;
              end;
            end;
          end;

          if DeviceData.rowdata <> '' then
          begin

            if not DatabaseConnected then
            begin
              ToLog('Error processing server message: DB does not connected', LOG_STD, LogFile, FormatSettings);
              Exit;
            end;

            extid := DeviceData.DevEui;
            ToLog('DevEui:' + extid, LOG_STD, LogFile, FormatSettings);
            DeviceData.id_terminal := EmptyStr;

            q.Close;
            q.SQL.Clear;
            q.SQL.Add('select * from terminals where ext_id = ' + QuotedStr(extid) );
            q.Open;
            if not q.IsEmpty then
            begin
              ToLog('Device exists: ' + extid, LOG_STD, LogFile, FormatSettings);
              DeviceData.id_terminal := q.FieldByName('id').AsString;
            end else
            begin
              q.Close;
              q.SQL.Clear;
              q.SQL.Add('insert into terminals(ext_id,id_place,id_core,id_terminals_type,id_terminals_model,lastconnect,state,name,rssi,port,capacity)');
              q.SQL.Add('values(' + QuotedStr(extid) + ',0,1,1,32,CURRENT_TIMESTAMP,1,' + QuotedStr(DeviceData.devName) +
               ', ' + DeviceData.rssi + ',1,' + DeviceData.DataPackge.BatteryCharge + ')');
              q.Execute;
              ToLog('Device added: ' + extid, LOG_STD, LogFile, FormatSettings);

              q.Close;
              q.SQL.Clear;
              q.SQL.Add('select last_insert_id() la');
              q.Open;
              if not q.Eof then
                DeviceData.id_terminal := q.FieldByName('la').AsString;
            end;

            if DeviceData.id_terminal <> EmptyStr then
            begin
              q.Close;
              q.SQL.Clear;
              q.SQL.Add('insert into terminals_data(date_server,id_place,id_gate,id_provider,state,id_terminal,rowdata,rssi,date_terminal,value,channel)');
              q.SQL.Add('values(' + QuotedStr(DeviceData.DataPackge.DataDateTime) + ',0,1,1,1,' + DeviceData.id_terminal + ',' +
                QuotedStr(DeviceData.rowdata) + ',' +
                DeviceData.rssi + ',' + QuotedStr(DeviceData.DataPackge.DataDateTime) + ',' +
                DeviceData.rowdata + ',1)');
              q.Execute;

              q.Close;
              q.SQL.Clear;
              q.SQL.Add('update terminals set lastconnect = CURRENT_TIMESTAMP, lastdata = ' +
                QuotedStr(DeviceData.DataPackge.DataDateTime) + ', capacity = ' +
                DeviceData.DataPackge.BatteryCharge +
                ' where id = ' + DeviceData.id_terminal);
              q.Execute;
            end;
          end;

          if DeviceData.DataPackge.Data1 <> '' then
          begin

            if not DatabaseConnected then
            begin
              ToLog('Error processing server message: DB does not connected', LOG_STD, LogFile, FormatSettings);
              Exit;
            end;

            extid := DeviceData.DevEui;
            ToLog('DevEui:' + extid, LOG_STD, LogFile, FormatSettings);
            DeviceData.id_terminal := EmptyStr;

            q.Close;
            q.SQL.Clear;
            q.SQL.Add('select * from terminals where ext_id = ' + QuotedStr(extid + '01') );
            q.Open;
            if not q.IsEmpty then
            begin
              ToLog('Device exists: ' + extid + '01', LOG_STD, LogFile, FormatSettings);
              DeviceData.id_terminal := q.FieldByName('id').AsString;
            end else
            begin
              q.Close;
              q.SQL.Clear;
              q.SQL.Add('insert into terminals(ext_id,id_place,id_core,id_terminals_type,id_terminals_model,lastconnect,state,name,rssi,port,capacity)');
              q.SQL.Add('values(' + QuotedStr(extid + '01') + ',0,1,1,32,CURRENT_TIMESTAMP,1,' + QuotedStr(DeviceData.devName) +
               ', ' + DeviceData.rssi + ',1,' + DeviceData.DataPackge.BatteryCharge + ')');
              q.Execute;
              ToLog('Device added: ' + extid + '01', LOG_STD, LogFile, FormatSettings);

              q.Close;
              q.SQL.Clear;
              q.SQL.Add('select last_insert_id() la');
              q.Open;
              if not q.Eof then
                DeviceData.id_terminal := q.FieldByName('la').AsString;
            end;

            if DeviceData.id_terminal <> EmptyStr then
            begin
              q.Close;
              q.SQL.Clear;
              q.SQL.Add('insert into terminals_data(date_server,id_place,id_gate,id_provider,state,id_terminal,rowdata,rssi,date_terminal,value,channel)');
              q.SQL.Add('values(' + QuotedStr(DeviceData.DataPackge.DataDateTime) + ',0,1,1,1,' + DeviceData.id_terminal + ',' +
                QuotedStr(DeviceData.DataPackge.Data1) + ',' +
                DeviceData.rssi + ',' + QuotedStr(DeviceData.DataPackge.DataDateTime) + ',' +
                DeviceData.DataPackge.Data1 + ',1)');
              q.Execute;

              q.Close;
              q.SQL.Clear;
              q.SQL.Add('update terminals set lastconnect = CURRENT_TIMESTAMP, lastdata = ' +
                QuotedStr(DeviceData.DataPackge.DataDateTime) + ', capacity = ' +
                DeviceData.DataPackge.BatteryCharge +
                ' where id = ' + DeviceData.id_terminal);
              q.Execute;
            end;
          end;

          if DeviceData.DataPackge.Data2 <> '' then
          begin

            if not DatabaseConnected then
            begin
              ToLog('Error processing server message: DB does not connected', LOG_STD, LogFile, FormatSettings);
              Exit;
            end;

            extid := DeviceData.DevEui;
            ToLog('DevEui:' + extid, LOG_STD, LogFile, FormatSettings);
            DeviceData.id_terminal := EmptyStr;

            q.Close;
            q.SQL.Clear;
            q.SQL.Add('select * from terminals where ext_id = ' + QuotedStr(extid + '02') );
            q.Open;
            if not q.IsEmpty then
            begin
              ToLog('Device exists: ' + extid + '02', LOG_STD, LogFile, FormatSettings);
              DeviceData.id_terminal := q.FieldByName('id').AsString;
            end else
            begin
              q.Close;
              q.SQL.Clear;
              q.SQL.Add('insert into terminals(ext_id,id_place,id_core,id_terminals_type,id_terminals_model,lastconnect,state,name,rssi,port,capacity)');
              q.SQL.Add('values(' + QuotedStr(extid + '02') + ',0,1,1,32,CURRENT_TIMESTAMP,1,' + QuotedStr(DeviceData.devName) +
               ', ' + DeviceData.rssi + ',1,' + DeviceData.DataPackge.BatteryCharge + ')');
              q.Execute;
              ToLog('Device added: ' + extid + '02', LOG_STD, LogFile, FormatSettings);

              q.Close;
              q.SQL.Clear;
              q.SQL.Add('select last_insert_id() la');
              q.Open;
              if not q.Eof then
                DeviceData.id_terminal := q.FieldByName('la').AsString;
            end;

            if DeviceData.id_terminal <> EmptyStr then
            begin
              q.Close;
              q.SQL.Clear;
              q.SQL.Add('insert into terminals_data(date_server,id_place,id_gate,id_provider,state,id_terminal,rowdata,rssi,date_terminal,value,channel)');
              q.SQL.Add('values(' + QuotedStr(DeviceData.DataPackge.DataDateTime) + ',0,1,1,1,' + DeviceData.id_terminal + ',' +
                QuotedStr(DeviceData.DataPackge.Data2) + ',' +
                DeviceData.rssi + ',' + QuotedStr(DeviceData.DataPackge.DataDateTime) + ',' +
                DeviceData.DataPackge.Data2 + ',1)');
              q.Execute;

              q.Close;
              q.SQL.Clear;
              q.SQL.Add('update terminals set lastconnect = CURRENT_TIMESTAMP, lastdata = ' +
                QuotedStr(DeviceData.DataPackge.DataDateTime) + ', capacity = ' +
                DeviceData.DataPackge.BatteryCharge +
                ' where id = ' + DeviceData.id_terminal);
              q.Execute;
            end;
          end;

        finally
          Pairs.Free;
        end;

      end;
    end;
  except
    on E: Exception do
      ToLog('Error processing server message:' + E.Message, LOG_STD, LogFile, FormatSettings);
  end;
end;

procedure TMQTT.MQTTTimerTimer(Sender: TObject);
var
  SStatus: String;
begin
  try
    SStatus := 'Unknown Error';
    case MQTTClient.ConnectionStatus of
      csNotConnected: SStatus := 'Not Connected';
      csConnectionRejected_InvalidProtocolVersion: SStatus := 'Connection Rejected Invalid Protocol Version';
      csConnectionRejected_InvalidIdentifier: SStatus := 'Connection Rejected Invalid Identifier';
      csConnectionRejected_ServerUnavailable: SStatus := 'Connection Rejected Server Unavailable';
      csConnectionRejected_InvalidCredentials: SStatus := 'Connection Rejected Invalid Credentials';
      csConnectionRejected_ClientNotAuthorized: SStatus := 'Connection Rejected Client Not Authorized';
      csConnectionLost: SStatus := 'Connection Lost';
      csConnecting: SStatus := 'Connecting';
      csReconnecting: SStatus := 'Reconnecting';
      csConnected: SStatus := 'Connected';
    end;
    ToLog('MQTTClient.ConnectionStatus1: ' + SStatus, LOG_STD, LogFile, FormatSettings);

    if MQTTClient.ConnectionStatus <> csConnected then
    begin

      SStatus := 'Unknown Error';
      case MQTTClient.ConnectionStatus of
        csNotConnected: SStatus := 'Not Connected';
        csConnectionRejected_InvalidProtocolVersion: SStatus := 'Connection Rejected Invalid Protocol Version';
        csConnectionRejected_InvalidIdentifier: SStatus := 'Connection Rejected Invalid Identifier';
        csConnectionRejected_ServerUnavailable: SStatus := 'Connection Rejected Server Unavailable';
        csConnectionRejected_InvalidCredentials: SStatus := 'Connection Rejected Invalid Credentials';
        csConnectionRejected_ClientNotAuthorized: SStatus := 'Connection Rejected Client Not Authorized';
        csConnectionLost: SStatus := 'Connection Lost';
        csConnecting: SStatus := 'Connecting';
        csReconnecting: SStatus := 'Reconnecting';
        csConnected: SStatus := 'Connected';
      end;
      ToLog('MQTTClient.ConnectionStatus2: ' + SStatus, LOG_STD, LogFile, FormatSettings);

      ToLog('Timer try to connect...', LOG_STD, LogFile, FormatSettings);
      if Self.FindComponent('MQTTClient') <> nil then
        Self.FindComponent('MQTTClient').Free;
      DiconnectFromDB;
      ConnectToServer;
      ToLog('Timer after connect.', LOG_STD, LogFile, FormatSettings);
    end;
  except
    on E: Exception do
      ToLog('Timer error: ' + E.Message, LOG_STD, LogFile, FormatSettings);
  end;
end;

procedure TMQTT.ServiceExecute(Sender: TService);
begin
  if not CommonStartupProcedure then
  begin
    CommonShutdownProcedure;
    Exit;
  end;
  while not Terminated do
    ServiceThread.ProcessRequests(True);
end;

procedure TMQTT.ServiceStart(Sender: TService; var Started: Boolean);
begin
  hHdl := CreateMutex(nil, True, 'MQTT_SERVER');
  if GetLastError = ERROR_ALREADY_EXISTS then
    Started := False;
end;

procedure TMQTT.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  ReleaseMutex(hHdl);
  CommonShutdownProcedure;
end;

procedure TMQTT.DBConnectionError(Sender: TObject; E: EDAError;
  var Fail: Boolean);
begin
  ToLog('Database error: ' + E.Message, LOG_STD, LogFile, FormatSettings);
  Fail := True;
end;

function HexToInt(s : String): Integer;
var
  i, r : Integer;
begin
  val('$' + Trim(s), r, i);
  if i <> 0 then Result := 0 //Ошибка в написании числа
  else Result := r;
end;

end.
